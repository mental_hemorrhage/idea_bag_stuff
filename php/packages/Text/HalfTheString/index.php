<?php

if (!isset($argv[ 1 ]) || empty($argv[ 1 ])) {
    echo 'Invalid argument passed to script. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];
$length = \strlen($argument);

if ($length % 2 !== 0) {
    echo 'The string has not an even length. Length is ' . $length . PHP_EOL;
    echo $argument . PHP_EOL;
    exit();
}

$firstPart = \substr($argument, 0, $length / 2);
$secondPart = \substr($argument, -($length / 2));

echo 'First part of string : ' . $firstPart . PHP_EOL;
echo 'Second part of string: ' . $secondPart . PHP_EOL;