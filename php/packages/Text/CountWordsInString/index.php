<?php

if (!isset($argv[ 1 ]) || empty($argv[ 1 ])) {
    echo 'Invalid argument passed to script. Script will end!' . PHP_EOL;
    exit();
}

$fileArgument = $argv[ 1 ];

if (!\file_exists($fileArgument)) {
    echo 'File does not exist. Script will end';
    exit();
}

$toSearch = [':', '&', '.', '^'];

$contents = \file_get_contents($fileArgument);

if (\strlen($contents) === 0) {
    echo 'File is empty. Script will end';
    exit();
}

$words = 0;
$paragraphs = 0;

if (false === \strpos($contents, "\n")) {
    $data[] = $contents;
} else {
    $data = \explode("\n", $contents);
}

foreach ($data as $key => $value) {
    $value = \str_replace($toSearch, ' ', $value);
    $value = \str_replace('  ', ' ', $value);
    $explodedText = \explode(' ', $value);

    if (\strlen($value) === 0) {
        ++$paragraphs;
    }

    $words += \count($explodedText);
}

echo 'The file contains ' . $paragraphs . ' paragraphs' . "\n";
echo 'The file contains ' . $words . ' words' . "\n";