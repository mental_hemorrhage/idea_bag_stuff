<?php

if (!isset($argv[ 1 ]) || empty($argv[ 1 ])) {
    echo 'Invalid argument passed to script. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];
$length = \strlen($argument);

if ($length < 3) {
    echo 'String ' . $argument . ' is too short. No palindrome possible. At least three characters are required.' . PHP_EOL;
    exit();
}

--$length;

for ($i = 0; $i <= $length; ++$i) {
    $firstCharacter = $argument[ $i ];
    $lastCharacter = $argument[ $length - $i ];

    if ($firstCharacter !== $lastCharacter) {
        echo $argument . ' is no palindrome. Script will end' . PHP_EOL;
        exit();
    }
}

echo $argument . ' is a palindrome' . PHP_EOL;
