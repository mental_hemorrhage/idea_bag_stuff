<?php

if (!isset($argv[ 1 ]) || empty($argv[ 1 ])) {
    echo 'Invalid argument passed to script. Script will end!' . PHP_EOL;
    exit();
}

$vowels = ['a' => 0, 'e' => 0, 'i' => 0, 'o' => 0, 'u' => 0,];
$hits = 0;

$argument = $argv[ 1 ];
$length = \strlen($argument);

if ($length < 3) {
    echo 'String ' . $argument . ' is too short. No palindrome possible. At least three characters are required.' . PHP_EOL;
    exit();
}

--$length;

for ($i = 0; $i <= $length; ++$i) {
    $character = \strtolower($argument[ $i ]);

    if (isset($vowels[ $character ])) {
        ++$vowels[ $character ];
        ++$hits;
    }
}

if ($hits === 0) {
    echo 'No vowels found. Script will end!' . PHP_EOL;
    exit();
}

foreach ($vowels as $key => $vowel) {
    if ($vowel === 0) {
        continue;
    }

    echo $key . ' ' . $vowel . PHP_EOL;
}