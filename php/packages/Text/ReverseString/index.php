<?php

if (!isset($argv[ 1 ]) || empty($argv[ 1 ])) {
    echo 'Invalid argument given. Script will end.';
    exit();
}

$argument = $argv[ 1 ];
$length = \strlen($argument);
$reversed = '';

for ($i = $length; $i >= 0; --$i) {
    if (isset($argument[ $i ])) {
        $reversed .= $argument[ $i ];
    }
}

echo 'The original string is ' . $argument . PHP_EOL;
echo 'The reversed string is ' . $reversed . PHP_EOL;