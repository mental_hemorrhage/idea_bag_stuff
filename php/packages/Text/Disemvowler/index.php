<?php
if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid string as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];
$vowels = ['a' => true, 'e' => true, 'i' => true, 'o' => true, 'u' => true,];
$collection = [];
$length = \strlen($argument);
$newString = '';
$vowelString = '';

for ($i = 0; $i < $length; ++$i) {
    $character = \strtolower($argument[ $i ]);

    if (!isset($vowels[ $character ])) {
        $newString .= $character;
    } else {
        $vowelString .= $character;
    }
}

echo 'This would be the new string: ' . PHP_EOL;
echo $newString . ' ' . $vowelString . PHP_EOL;