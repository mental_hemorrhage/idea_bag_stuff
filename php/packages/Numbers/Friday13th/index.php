<?php

if ((!isset($argv[ 1 ]) && empty($argv[ 1 ])) && (!isset($argv[ 2 ]) && empty($argv[ 2 ])) && (!isset($argv[ 3 ]) && empty($argv[ 3 ]))) {
    echo 'No valid elements as argument. Script will end!' . PHP_EOL;
    exit();
}

function properDateFormatting($dateFormat, $dateString)
{
    return \date($dateFormat, \strtotime($dateString));
}

function dateIncrement($dateFormat, $dateString, $dateStart)
{
    return \date($dateFormat, \strtotime($dateString, \strtotime($dateStart)));
}

$dateFormat = 'Y-m-d';
$dateStart = date($dateFormat);
$dateEnd = date($dateFormat);
$fridays = [];

if (isset($argv[ 1 ]) && (!isset($argv[ 2 ]) && !isset($argv[ 3 ]))) {
    $dateStart = properDateFormatting($dateFormat, $argv[ 1 ] . '-01-01');
    $dateEnd = properDateFormatting($dateFormat, $argv[ 1 ] . '-12-31');
} else {
    $dateStart = !empty($argv[ 2 ]) ? properDateFormatting($dateFormat, $argv[ 2 ]) : $dateStart;
    $dateEnd = !empty($argv[ 3 ]) ? properDateFormatting($dateFormat, $argv[ 3 ]) : $dateEnd;
}

$weekDay = properDateFormatting('w', $dateStart);

if (5 < $weekDay) {
    $diff = $weekDay - 5;
} elseif ($weekDay === 5) {
    $diff = 0;
} else {
    $diff = 5 - $weekDay;
}

$string = '+' . $diff . ' days';
$dateStart = dateIncrement($dateFormat, $string, $dateStart);

while (true) {
    $day = properDateFormatting('d', $dateStart);

    if ((int)$day === 13) {
        $fridays[] = $dateStart;
    }

    if (\strtotime($dateStart) > \strtotime($dateEnd)) {
        break;
    }

    $dateStart = dateIncrement($dateFormat, '+7 days', $dateStart);
}

echo 'Fridays the 13th are ' . \implode(', ', $fridays) . PHP_EOL;
