<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$exponent = 0;
$hits = [];

while (true) {
    if (\pow(2, $exponent) < $argument) {
        ++$exponent;
        continue;
    }

    break;
}

--$exponent;

for ($i = $exponent; $i >= 0; --$i) {
    $hit = 0;
    $powValue = \pow(2, $i);

    if ($argument >= $powValue) {
        $argument -= $powValue;
        $hit = 1;
    }

    $hits[] = $hit;
}

$hitsValue = \implode($hits, '');

echo 'PHP Function result   : ' . \decbin($argv[ 1 ]) . PHP_EOL;
echo 'Algorithm result      : ' . $hitsValue . PHP_EOL;

$recalculated = 0;
$length = \strlen($hitsValue);
--$length;

for ($i = 0; $i <= $length; ++$i) {
    $charValue = (int)$hitsValue[ $length - $i ];

    if (0 === $charValue) {
        $recalculated += 0;
        continue;
    }

    $recalculated += \pow(2, $i);
}

echo 'Original value        : ' . $argv[ 1 ] . PHP_EOL;
echo 'Recalculated value    : ' . $recalculated . PHP_EOL;
