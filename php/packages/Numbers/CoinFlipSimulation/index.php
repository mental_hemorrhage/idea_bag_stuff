<?php
if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid string as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$result = [0 => 0, 1 => 0];

for ($i = 0; $i < $argument; ++$i) {
    $flip = \mt_rand(0, 1);
    ++$result[ $flip ];
}

echo 'Heads: ' . $result[ 0 ] . PHP_EOL;
echo 'Tails: ' . $result[ 1 ] . PHP_EOL;
