<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$exponent = 0;
$factorString = 'factor';
$valueString = 'value';
$hits = [];
$hexToDec = ['a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15];
$decToHex = [10 => 'a', 11 => 'b', 12 => 'c', 13 => 'd', 14 => 'e', 15 => 'e'];

while (true) {
    if (\pow(16, $exponent) < $argument) {
        ++$exponent;
        continue;
    }

    break;
}

--$exponent;

for ($i = $exponent; $i >= 0; --$i) {
    $tmp = [$factorString => 0, $valueString => 0];
    $powValue = \pow(16, $i);

    if ($argument >= $powValue) {
        $factor = (int)($argument / $powValue);
        $tmp[ $factorString ] = $decToHex[ $factor ] ?? $factor;
        $tmp[ $valueString ] = $i;

        $argument -= $powValue * $factor;
    }

    $hits[] = $tmp;
}

$hitsValue = '';

foreach ($hits as $hit) {
    $hitsValue .= $hit[ $factorString ];
}

echo 'PHP Function result   : ' . \dechex($argv[ 1 ]) . PHP_EOL;
echo 'Algorithm result      : ' . $hitsValue . PHP_EOL;

$recalculated = 0;

foreach ($hits as $hit) {
    $recalculated += ($hexToDec[ $hit[ $factorString ] ] ?? $hit[ $factorString ]) * \pow(16, $hit[ $valueString ]);
}
echo 'Original value        : ' . $argv[ 1 ] . PHP_EOL;
echo 'Recalculated value    : ' . $recalculated . PHP_EOL;
