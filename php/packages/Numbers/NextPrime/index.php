<?php

echo 'Please end a prime number: ';
$firstRun = true;
$currentPrime = 0;
$isPrime = false;

while (true) {
    $handle = \fopen('php://stdin', 'r');
    $argument = \trim(\fgets($handle));

    if (!\is_numeric($argument) && $firstRun === true) {
        echo 'Not interested in prime numbers, ey? Well ... let us end this, then!' . PHP_EOL;
        exit;
    } elseif ($argument !== 'y' && $firstRun === false) {
        echo PHP_EOL . 'Enough prime numbers for now ... let us end this!' . PHP_EOL;
        exit;
    }

    if ($firstRun === true) {
        $currentPrime = (int)$argument;
    }

    \fclose($handle);

    if ($firstRun === true) {
        while (true) {
            for ($i = 2; $i < $currentPrime; ++$i) {
                if ($currentPrime % $i === 0) {
                    break(2);
                }
            }

            $isPrime = true;
            break;
        }

        $firstRun = false;
    } else {
        ++$currentPrime;

        while (true) {
            $looping = false;

            for ($i = 2; $i < $currentPrime; ++$i) {
                if ($currentPrime % $i === 0) {
                    $looping = true;
                    break;
                }
            }

            if ($looping === true) {
                ++$currentPrime;
                continue;
            }

            break;
        }
    }

    if ($firstRun === false) {
        $argument = $currentPrime;
    }

    $result = $argument . ' is not a prime.' . PHP_EOL;

    if ($isPrime === true) {
        $result = $argument . ' is a prime' . PHP_EOL;
    }

    echo $result;
    echo 'Are you still looking for prime numbers? (y/n)';
}

