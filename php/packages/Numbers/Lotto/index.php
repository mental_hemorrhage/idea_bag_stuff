<?php

$count = \count($argv);

if (7 !== $count) {
    echo 'Invalid amount of elements as argument. Expected 6, but received ' . --$count . ' Script will end!' . PHP_EOL;
    exit();
}

$numbers = [];
$lottoNumbers = [];
$hits = [];

for ($i = 1; $i < 7; ++$i) {
    $number = $argv[ $i ];

    if (!\is_numeric($number) && !\is_int($number)) {
        echo $number . ' is not numeric and not an integer. Script will end!' . PHP_EOL;
        exit();
    }

    if (1 > $number) {
        echo $number . ' is too small. Script will end!' . PHP_EOL;
        exit();
    }

    if (49 < $number) {
        echo $number . ' is too high . Script will end!' . PHP_EOL;
        exit();
    }

    $lottoNumbers[] = \mt_rand(1, 49);
    $numbers[] = $number;
}

$config = \json_decode(\file_get_contents('config.json'), true);

if (!\is_array($config) && !isset($config[ 'payout' ])) {
    echo 'config.json missing. Script will end!' . PHP_EOL;
    exit();
}

$history = \json_decode(\file_get_contents('history.json'), true);

if (!\is_array($history) && !isset($config[ 'history' ])) {
    echo 'history.json missing. Script will end!' . PHP_EOL;
    exit();
}

if (!isset($history[ 'history' ][ 'total_win' ])) {
    $history[ 'history' ][ 'total_win' ] = 0;
}

if (!isset($history[ 'history' ][ 'attempts' ])) {
    $history[ 'history' ][ 'attempts' ] = 0;
}

++$history[ 'history' ][ 'attempts' ];

foreach ($numbers as $number) {
    if (\in_array($number, $lottoNumbers)) {
        $hits[] = $number;

        if (!isset($history[ 'history' ][ 'hits' ][ $number ])) {
            $history[ 'history' ][ 'hits' ][ $number ] = 0;
        }

        ++$history[ 'history' ][ 'hits' ][ $number ];
    }
}

foreach ($lottoNumbers as $number) {
    if (!isset($history[ 'history' ][ 'picked' ][ $number ])) {
        $history[ 'history' ][ 'picked' ][ $number ] = 0;
    }

    ++$history[ 'history' ][ 'picked' ][ $number ];
}

$wins = \count($hits);

if (!isset($history[ 'history' ][ 'payouts' ][ $wins ])) {
    $history[ 'history' ][ 'payouts' ][ $wins ] = 0;
}

if (0 !== $wins) {
    ++$history[ 'history' ][ 'payouts' ][ $wins ];
}

$lottoWin = $config[ 'payout' ][ (string)$wins ];

$history[ 'history' ][ 'total_win' ] += $lottoWin;

\ksort($history[ 'history' ][ 'payouts' ]);
\ksort($history[ 'history' ][ 'hits' ]);
\ksort($history[ 'history' ][ 'picked' ]);

\file_put_contents('config.json', \json_encode($config));
\file_put_contents('history.json', \json_encode($history));

echo 'Hits: ' . $wins . ' with the numbers ' . \implode(', ', $hits) . PHP_EOL;
echo 'Payout: ' . $lottoWin . PHP_EOL;
echo 'Average win: ' . $history[ 'history' ][ 'total_win' ] / $history[ 'history' ][ 'attempts' ] . PHP_EOL;
