<?php

echo 'Please enter a number for the program to guess. Do not overdo and keep it in the range between 0 and 100 ' . PHP_EOL;
$handle = \fopen('php://stdin', 'r');
$number = \trim(\fgets($handle));
$attempts = 0;
$down = 0;
$top = 100;

if (!\is_numeric($number)) {
    echo 'Not interested in guessing numbers, ey? Well ... let us end this, then!' . PHP_EOL;
    exit;
}

\fclose($handle);

$number = (int)$number;
if (100 < $number) {
    echo 'Out of range? Well ... let us end this, then!' . PHP_EOL;
    exit;
}

while (true) {
    $guessedNumber = mt_rand($down, $top);

    echo 'The script guesses the number: ' . $guessedNumber . PHP_EOL;
    echo 'Please enter 0 for a correct guess, 1 for "too high" or 2 for "too low". Any other input will end the script.' . PHP_EOL;

    $handle = \fopen('php://stdin', 'r');
    $userResponse = \trim(\fgets($handle));
    \fclose($handle);

    if (!\is_numeric($userResponse)) {
        echo 'Not interested in guessing numbers, ey? Well ... let us end this, then!' . PHP_EOL;
        exit;
    }

    $userResponse = (int)$userResponse;

    if ($userResponse === 0) {
        echo 'The script has guessed the number correctly and it took ' . $attempts . ' attempts' . PHP_EOL;
        exit();
    } elseif ($userResponse === 1) {
        $top = $guessedNumber;
        --$top;
    } elseif ($userResponse === 2) {
        $down = $guessedNumber;
        ++$down;
    } else {
        echo 'Not interested in helping the program along, ey? Well ... let us end this, then!' . PHP_EOL;
        exit;
    }

    ++$attempts;
}

