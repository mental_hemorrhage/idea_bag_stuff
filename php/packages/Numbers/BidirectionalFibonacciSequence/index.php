<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$fibonacci = 1;
$first = 0;
$second = 1;
$display = '';

/**
 * The 1 is ignored in the loop because the Fibonacci number is one.
 */
for ($i = 2; $i <= $argument; $i++) {
    $fibonacci = $first + $second;
    $first = $second;
    $second = $fibonacci;

    $display = $fibonacci;

    if ($argument % 2 === 0) {
        $display = '-' . $display;
    }
}

echo 'The Fibonacci number for the number ' . $argument . ' is ' . $display . PHP_EOL;
