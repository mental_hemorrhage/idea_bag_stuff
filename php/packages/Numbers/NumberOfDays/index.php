<?php

if ((!isset($argv[ 1 ]) && empty($argv[ 1 ])) && (!isset($argv[ 2 ]) && empty($argv[ 2 ]))) {
    echo 'No valid elements as argument. Two numbers as arguments are expected. Script will end!' . PHP_EOL;
    exit();
}

$argument1 = $argv[ 1 ];
$argument2 = $argv[ 2 ];
$dateFormat = 'd.m.Y';

try {
    $date1 = new \DateTime($argument1);
    $date1->format($dateFormat);
} catch (\Exception $e) {
    echo $e->getTraceAsString();
    exit();
}

try {
    $date2 = new \DateTime($argument2);
    $date2->format($dateFormat);
} catch (\Exception $e) {
    echo $e->getTraceAsString();
    exit();
}

$diff = $date1->diff($date2);

echo 'Difference of days between ' . $argument1 . ' and ' . $argument2 . ' is ' . $diff->days . ' days.' . PHP_EOL;
