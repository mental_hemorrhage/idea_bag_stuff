<?php
/**
 * https://en.wikipedia.org/wiki/Approximations_of_%CF%80#Efficient_methods
 */

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$length = 10;

if (!isset($argv[ 2 ]) && ! empty($argv[ 2 ])) {
    $length = $argv[ 2 ];
}

$exp = \exp($argument);
$exp = \substr($exp, 0, ($length + 2));

echo 'e^x of ' . $argument . ' is ' . $exp . PHP_EOL;
