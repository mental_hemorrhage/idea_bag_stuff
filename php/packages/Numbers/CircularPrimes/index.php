<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$primes = [];
$argumentLength = \strlen((string)$argument);
$noPrimes = [2, 4, 6, 8, 5];

foreach ($noPrimes as $noPrime) {
    if ($argument % $noPrime === 0) {
        echo $argument . ' is not a circular prime' . PHP_EOL;
        echo 'Divisor is ' . $noPrime . PHP_EOL;
        exit();
    }
}

while (true) {
    for ($i = 2; $i < $argument; ++$i) {
        if ($argument % $i === 0) {
            echo $argument . ' is not a circular prime' . PHP_EOL;
            echo 'Divisor is ' . $i . PHP_EOL;
            exit();
        }

        $primes[ $argument ] = $argument;
    }

    if ($argumentLength === \count($primes)) {
        break;
    }

    $argument .= $argument[ 0 ];
    $argument = \substr($argument, 1, $argumentLength);
}

echo $argv[ 1 ] . ' is a circular prime' . PHP_EOL;
echo 'Primes are ' . \implode(', ', $primes) . PHP_EOL;
