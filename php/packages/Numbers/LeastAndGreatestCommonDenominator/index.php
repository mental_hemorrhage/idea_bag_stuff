<?php

if ((!isset($argv[ 1 ]) && empty($argv[ 1 ])) && (!isset($argv[ 2 ]) && empty($argv[ 2 ]))) {
    echo 'No valid elements as argument. Two numbers as arguments are expected. Script will end!' . PHP_EOL;
    exit();
}

$argument1 = $argv[ 1 ];
$argument2 = $argv[ 2 ];

if (!\is_numeric($argument1) && !\is_int($argument1)) {
    echo 'Argument1 is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

if (!\is_numeric($argument2) && !\is_int($argument2)) {
    echo 'Argument2 is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$smaller = $argument1;
$least = 1;
$greatest = 1;

if ($argument1 > $argument2) {
    $smaller = $argument2;
}

$denominator = [];

for ($i = 2; $i < $smaller; ++$i) {
    if ($argument1 % $i === 0) {
        $denominator[] = $i;
    }
}

foreach ($denominator as $item) {
    if ($argument2 % $item === 0 && $least > $item) {
        $least = $item;
        break;
    }
}

\rsort($denominator);

foreach ($denominator as $item) {
    if ($argument2 % $item === 0 && $greatest < $item) {
        $greatest = $item;
        break;
    }
}

echo 'Least: ' . $least . PHP_EOL;
echo 'Greatest: ' . $greatest . PHP_EOL;
