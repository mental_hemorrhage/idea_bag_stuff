<?php
/**
 * https://en.wikipedia.org/wiki/Approximations_of_%CF%80#Efficient_methods
 */

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$length = $argument + 10;

\ini_set('precision', $length);

$exploded = \explode('.', \pi());
$exploded[ 1 ] = \substr($exploded[ 1 ], 0, $argument);

$pi = \implode('.', $exploded);

echo 'PI to the ' . $argument . ' digit ' . $pi . PHP_EOL;
