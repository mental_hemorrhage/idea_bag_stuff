<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$exponent = 0;
$factorString = 'factor';
$valueString = 'value';

while (true) {
    if (\pow(8, $exponent) < $argument) {
        ++$exponent;
        continue;
    }

    break;
}

--$exponent;
$hits = [];

for ($i = $exponent; $i >= 0; --$i) {
    $tmp = [$factorString => 0, $valueString => 0];
    $powValue = \pow(8, $i);

    if ($argument >= $powValue) {
        $tmp[ $factorString ] = (int)($argument / $powValue);
        $tmp[ $valueString ] = $i;

        $argument -= $powValue * $tmp[ $factorString ];
    }

    $hits[] = $tmp;
}

$hitsValue = '';

foreach ($hits as $hit) {
    $hitsValue .= $hit[ $factorString ];
}

echo 'PHP Function result   : ' . \decoct($argv[ 1 ]) . PHP_EOL;
echo 'Algorithm result      : ' . $hitsValue . PHP_EOL;

$recalculated = 0;

foreach ($hits as $hit) {
    $recalculated += $hit[ $factorString ] * \pow(8, $hit[ $valueString ]);
}

echo 'Original value        : ' . $argv[ 1 ] . PHP_EOL;
echo 'Recalculated value    : ' . $recalculated . PHP_EOL;
