<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$happy = 0;

for ($j = 0; $j < 4; ++$j) {
    for ($i = 0; $i < \strlen($argument); ++$i) {
        $happy += $argument[ $i ] ** 2;
    }

    $argument = (string)$happy;
    $happy = 0;
}

echo 'The happy number of ' . $argv[ 1 ] . ' is ' . $argument . PHP_EOL;
