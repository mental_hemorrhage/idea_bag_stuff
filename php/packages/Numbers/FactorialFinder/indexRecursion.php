<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

/**
 * Recursive factorial function
 *
 * @param $value
 * @return float|int
 */
function factorial($value)
{
    if ($value > 0) {
        return $value * factorial($value - 1);
    }

    return 1;
}

$factorial = factorial($argument);
$factorial = \number_format($factorial, 0, ',', '.');

echo 'Factorial of ' . $argument . ' is ' . $factorial . PHP_EOL;
