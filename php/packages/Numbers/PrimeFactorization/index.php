<?php

if (!isset($argv[ 1 ]) && empty($argv[ 1 ])) {
    echo 'No valid element as argument. Script will end!' . PHP_EOL;
    exit();
}

$argument = $argv[ 1 ];

if (!\is_numeric($argument) && !\is_int($argument)) {
    echo 'Argument is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$primes = [];
$argument = (int)$argument;
$tmp = [];

for ($i = $argument; $i > 1; --$i) {
    if ($argument === $i) {
        continue;
    }

    if ($argument % $i === 0) {
        $primes[] = $i;
    }
}

while (true) {
    foreach ($primes as $key1 => $prime) {
        foreach ($primes as $key2 => $prime2) {
            if ($prime === $prime2) {
                continue;
            }

            if ($prime2 % $prime === 0) {
                $tmp[ $prime ] = $prime;
            }
        }
    }

    if (\count($tmp) === 0) {
        break;
    }

    $primes = $tmp;
    $tmp = [];
}

\sort($primes);

echo 'The prime factors for ' . $argument . ' are ' . \implode(', ', $primes) . PHP_EOL;
