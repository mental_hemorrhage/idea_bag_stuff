<?php

if ((!isset($argv[ 1 ]) && empty($argv[ 1 ])) && (!isset($argv[ 2 ]) && empty($argv[ 2 ]))) {
    echo 'No valid elements as argument. Two numbers as arguments are expected. Script will end!' . PHP_EOL;
    exit();
}

$argument1 = $argv[ 1 ];
$argument2 = $argv[ 2 ];

if (!\is_numeric($argument1) && !\is_int($argument1)) {
    echo 'Argument1 is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

if (!\is_numeric($argument2) && !\is_int($argument2)) {
    echo 'Argument2 is not numeric and not an integer. Script will end!' . PHP_EOL;
    exit();
}

$neonValues = [];

for ($i = $argument1; $i <= $argument2; ++$i) {
    $powered = \pow($i, 2);

    if ((int)\array_sum(\str_split((string)$powered)) === (int)$i) {
        $neonValues[] = $i;
    }
}

echo 'The neon values in the range ' . $argument1 . ' and ' . $argument2 . ' is ' . \implode(', ', $neonValues) . PHP_EOL;
